import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'

Vue.config.productionTip = false

// Buefy
Vue.use(Buefy, {
  defaultIconPack: 'fa'
})
// or
// Vue.component(Buefy.Checkbox.name, Buefy.Checkbox)
// Vue.component(Buefy.Table.name, Buefy.Table)
// Vue.component(Buefy.Switch.name, Buefy.Switch)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
